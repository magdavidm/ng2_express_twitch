//'use strict';

var System = require('systemjs');

//import System from 'systemjs';

//Services
import { TwitchService } from './server/twitch.service';
let twitchService = new TwitchService();

//Modules
var express = require('express');

var app = express();
const $https = require('https');
var path = require('path');
var rootPath = path.normalize(__dirname);
var nodePort = '1234';

app.use(express.static(rootPath + '/client'));
//console.log(rootPath);
app.use('/node_modules', express.static(rootPath + '/node_modules')) //this exposes node modules to client, DO NOT do in production


app.get('/', function (req, res) {
    res.sendFile(rootPath + '/index.html');
});
app.get('/dashboard', function (req, res) {
    res.send("Server dashboard Response");
    // res.sendFile(rootPath + '/index.html');
});
app.get('/singleTwitch', function (req, res) {
    res.send("Server singleTwitch Response");
});
app.get('/searchUser/:term', function (req, res) {
    console.log("searching for user...");
    var searchTerm = req.params.term;
    twitchService.retrieveUser(searchTerm, function(body, err){
        console.log("done");
        if (err) throw err;
        res.send(body);
    })
});
app.get('/searchUserChn/:term', function (req, res) {
    console.log("searching for user channel...");
    var searchTerm = req.params.term;
    twitchService.retrieveUserChannel(searchTerm, function(body, err){
        console.log("done");
        if (err) throw err;
        res.send(body);
    })
});
app.get('/search/:term', function (req, res) {
    console.log("searching...");
    var searchTerm = req.params.term;
    twitchService.retrieveChannels(searchTerm, function(body, err){
        console.log("done");
        if (err) throw err;
        res.send(body);
    })
});
app.get('/streams/:term', function (req, res) {
    console.log("searching...");
    var searchTerm = req.params.term;
    twitchService.retrieveStreamData(searchTerm, function(body, err){
        console.log("done");
        if (err) throw err;
        res.send(body);
    })
});

app.listen(nodePort);
console.log(new Date() + ' Listening on port: ' + nodePort);