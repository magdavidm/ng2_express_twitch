/* eslint-env node */

'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var del = require('del');
var express = require('express');
// var ghPages = require('gh-pages');
var packageJson = require('./package.json');
var path = require('path');
// var runSequence = require('run-sequence');
// var swPrecache = require('../lib/sw-precache.js');
var swPrecache = require('sw-precache');
var exec = require('child_process').exec;

var DEV_DIR = 'client';
var DIST_DIR = 'dist';

gulp.task('generate-service-worker', function (callback) {
  var path = require('path');
  var swPrecache = require('sw-precache');
  var rootDir = 'client';
  var config = {
    "staticFileGlobs": [
      rootDir + '/**/*.{js,html,css,png,jpg,gif}',
      "client/twitch/**.html"
    ],
    "stripPrefix": rootDir
  }
  swPrecache.write(path.join(rootDir, 'service-worker.js'), config, callback);
});

gulp.task('serve-dev', ['generate-service-worker-dev'], function () {
  runExpress(3001, DEV_DIR);
});

gulp.task('serve', callback => {
  exec('node index.js', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    // cb(err);
  })
});

gulp.task('default', function () {
  gulp.run('generate-service-worker');
  gulp.run('serve');
})