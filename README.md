### ng2_express_twitch
#### Description
This is a simple reference of using [Angular 2 RC1](http://angular.io) with an [Express](http://expressjs.com) server.

This was started from [NG2 Express](https://github.com/CoolHandDev/ng2-express) GitHub page.

Because Node uses ```CommonJS``` as the native module system and not ```SystemJS```, may need to make modifications to the ```configuration.js``` that configures SystemJS.

This example uses TypeScript on both client and server side.

The client initiates a search for a user name, that is passed on a "GET" request to the server, which uses the TwitchTV API to get information.

As with the project this started from, it is worth repeating that "Exposing node_modules is not a good idea.  In your build process, place dependencies in a library folder and adjust SystemJS config and script imports accordingly."