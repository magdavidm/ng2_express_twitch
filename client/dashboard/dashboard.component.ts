import { Component } from '@angular/core';

@Component({
    selector: 'user-dashboard',
    templateUrl: 'dashboard/dashboard.component.html',
})
export class DashboardComponent {
    pressedBtn = 0;

    buttonDetail() {
        this.pressedBtn ++;        
        console.log("something " + this.pressedBtn);
    }

}

