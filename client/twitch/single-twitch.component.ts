import { Component } from '@angular/core';
import { RouteParams } from '@angular/router-deprecated';

import { TwitchUser } from './twitch-user';
// import TwitchSharedService = require('./twitch-shared.service');

@Component({
    selector: 'single-twitch',
    templateUrl: 'twitch/single-twitch.component.html',
    styleUrls: ['twitch/single-twitch.component.css']
})
export class SingleTwitchComponent {
    //user: TwitchUser;
    navigated = false; // true if navigated here
    searchInput = "";
    output = "";
    displayName = "";
    currentGame = "";
    statusMessage: string;
    searchSuccess = "User Retrieved";
    searchingBool = false; 
    someUser: TwitchUser;
    chnSearched = false;
    constructor(
        ) { }
    resetStatusValues() {
        this.displayName = "";
        this.currentGame = "";
        this.chnSearched = false;
    }
    searchUser() {
        var app = this;
        this.resetStatusValues();
        app.someUser = {
            display_name: "",
            broadcaster_language: "",
            channel_link: "",
            current_game: "",
            views: 0,
            followers: 0,
            logo: "",
            fresh_data: false,
            last_updated: "",
        };
        app.searchingBool = true;
        app.displayName = app.searchInput;
        app.statusMessage = "Searching..."
        app.output = app.searchInput + " " + "Clicked";
        var searchUrl = "/searchUser/" + app.searchInput;
        this.callAjax(searchUrl, function (msg) {
            var jsonMsg = JSON.parse(msg);
            //console.log(jsonMsg);
            //console.log(typeof(jsonMsg));
            //console.log(JSON.stringify(jsonMsg));
            // console.log(jsonMsg.channels[0].mature);
            //app.user.display_name = jsonMsg.display_name;
            //app.user.current_game = " ";
            if (jsonMsg.error) {
                console.log("Error: " + jsonMsg.error);
                console.log("No user by that name...");
                app.statusMessage = "No user by that name...";
            }
            else {
                app.displayName = jsonMsg.display_name;
                app.statusMessage = app.searchSuccess;

                app.someUser.display_name = jsonMsg.display_name;
                app.someUser.current_game = jsonMsg.game;
                if(!jsonMsg.game){
                    app.someUser.current_game = "Offline"
                }
                app.someUser.views = jsonMsg.views;
                app.someUser.channel_link = jsonMsg.url;
                app.someUser.last_updated = jsonMsg.updated_at;
            }
            console.log("displayName: " + app.displayName);
            console.log(msg);
            app.searchingBool = false;
        });
    }
    searchUserChn() {
        var app = this;
        app.someUser = {
            display_name: "",
            broadcaster_language: "",
            channel_link: "",
            current_game: "",
            views: 0,
            followers: 0,
            logo: "",
            fresh_data: false,
            last_updated: "",
        };
        this.resetStatusValues();
        app.searchingBool = true;
        app.chnSearched = true;
        app.displayName = app.searchInput;
        app.statusMessage = "Searching..."
        var searchUrl = "/searchUserChn/" + app.searchInput;
        this.callAjax(searchUrl, function (msg) {
            var jsonMsg = JSON.parse(msg);
            if (jsonMsg.error) {
                // console.log("Error: " + jsonMsg.error);
                // console.log("No user by that name...");
                app.statusMessage = "No user by that name...";
            }
            else {
                app.displayName = jsonMsg.display_name;
                app.currentGame = "Offline";
                app.statusMessage = app.searchSuccess;
                
                app.someUser.display_name = jsonMsg.display_name;
                app.someUser.current_game = jsonMsg.game;
                if (jsonMsg.game == null){
                    app.someUser.current_game = "Offline";
                }
                app.someUser.views = jsonMsg.views;
                app.someUser.channel_link = jsonMsg.url;
                app.someUser.last_updated =  jsonMsg.updated_at;
                
                if (jsonMsg.status !== null) {
                    app.currentGame = "Streaming";
                }
                // TwitchSharedVars.
            }
            // console.log("displayName: " + app.displayName);
            app.searchingBool = false;
            console.log(msg);
        });
    }
    searchStreams(){
    }
    callAjax(url, callback) {
        var xmlhttp;
        // compatible with IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                callback(xmlhttp.responseText);
            }
        }
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }
}
