import { Component } from '@angular/core';
import { RouteParams } from '@angular/router-deprecated';

import { TwitchUser } from './twitch-user';
import { TwitchService } from './twitch.service';

// import TwitchSharedService = require('./twitch-shared.service');
import { TwitchSharedService } from './twitch-shared.service';

@Component({
    selector: 'twitch-stream-multi',
    templateUrl: 'twitch/twitch-stream-multi.component.html',
    styleUrls: ['twitch/twitch-stream-multi.component.css']
})

export class TwitchStreamMultiComponent {
    constructor(
        private twitchService: TwitchService,
        private twitchSharedService: TwitchSharedService
    ) { }
    usersArr = this.twitchSharedService.getNameList();
    testStr = this.twitchSharedService.testStr;
    //sUser = TwitchUser;
    nameInput: string;
    singleUsr: TwitchUser;

    addUser() {
        // console.log(app.usersArr.length);
        var app = this;
        var user = new TwitchUser();
        var testUsr = {
            display_name: app.nameInput,
            status: "Unknown"
        };
        user.display_name = app.nameInput;
        app.usersArr.push(testUsr);
        console.log(app.usersArr.length);
    }
    getStreamData() {
        var app = this;
        for (let usrNum in app.usersArr) {
            app.usersArr[usrNum].status = "Unknown";
        }
        for (let usrNum in app.usersArr) {
            let url = '/streams/' + app.usersArr[usrNum].display_name;
            this.twitchService.callAjax(url, function (msg) {
                var msgAsJSON = JSON.parse(msg);
                console.log(msg);
                if (msgAsJSON.stream !== null) {
                    app.usersArr[usrNum].status = msgAsJSON.stream.game;
                }
                else {
                    app.usersArr[usrNum].status = "Offline";
                }

            })
        }
    }
}