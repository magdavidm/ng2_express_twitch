"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var twitch_user_1 = require('./twitch-user');
var twitch_service_1 = require('./twitch.service');
var TwitchUsersComponent = (function () {
    function TwitchUsersComponent(twitchService) {
        this.twitchService = twitchService;
    }
    TwitchUsersComponent.prototype.searchUsers = function () {
        var app = this;
        var url = '/search/' + this.searchTerm;
        app.tUsers = [];
        //        console.log(url);
        this.twitchService.callAjax(url, function (msg) {
            var msgAsJSON = JSON.parse(msg);
            var arrayUsers = [];
            //console.log(msg);
            for (var i in msgAsJSON.channels) {
                var user = new twitch_user_1.TwitchUser;
                user.display_name = msgAsJSON.channels[i].display_name;
                user.current_game = msgAsJSON.channels[i].game;
                user.followers = msgAsJSON.channels[i].followers;
                user.views = msgAsJSON.channels[i].views;
                user.logo = msgAsJSON.channels[i].logo;
                //arrayUsers.push(user);
                app.tUsers.push(user);
            }
            //app.tUsers = arrayUsers;
        });
    };
    TwitchUsersComponent = __decorate([
        core_1.Component({
            selector: 'twitch-users',
            templateUrl: 'twitch/twitch-users.component.html',
            styleUrls: ['twitch/twitch-users.component.css']
        }), 
        __metadata('design:paramtypes', [twitch_service_1.TwitchService])
    ], TwitchUsersComponent);
    return TwitchUsersComponent;
}());
exports.TwitchUsersComponent = TwitchUsersComponent;
//# sourceMappingURL=twitch-users.component.js.map