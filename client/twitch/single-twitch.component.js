"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
// import TwitchSharedService = require('./twitch-shared.service');
var SingleTwitchComponent = (function () {
    function SingleTwitchComponent() {
        //user: TwitchUser;
        this.navigated = false; // true if navigated here
        this.searchInput = "";
        this.output = "";
        this.displayName = "";
        this.currentGame = "";
        this.searchSuccess = "User Retrieved";
        this.searchingBool = false;
        this.chnSearched = false;
    }
    SingleTwitchComponent.prototype.resetStatusValues = function () {
        this.displayName = "";
        this.currentGame = "";
        this.chnSearched = false;
    };
    SingleTwitchComponent.prototype.searchUser = function () {
        var app = this;
        this.resetStatusValues();
        app.someUser = {
            display_name: "",
            broadcaster_language: "",
            channel_link: "",
            current_game: "",
            views: 0,
            followers: 0,
            logo: "",
            fresh_data: false,
            last_updated: "",
        };
        app.searchingBool = true;
        app.displayName = app.searchInput;
        app.statusMessage = "Searching...";
        app.output = app.searchInput + " " + "Clicked";
        var searchUrl = "/searchUser/" + app.searchInput;
        this.callAjax(searchUrl, function (msg) {
            var jsonMsg = JSON.parse(msg);
            //console.log(jsonMsg);
            //console.log(typeof(jsonMsg));
            //console.log(JSON.stringify(jsonMsg));
            // console.log(jsonMsg.channels[0].mature);
            //app.user.display_name = jsonMsg.display_name;
            //app.user.current_game = " ";
            if (jsonMsg.error) {
                console.log("Error: " + jsonMsg.error);
                console.log("No user by that name...");
                app.statusMessage = "No user by that name...";
            }
            else {
                app.displayName = jsonMsg.display_name;
                app.statusMessage = app.searchSuccess;
                app.someUser.display_name = jsonMsg.display_name;
                app.someUser.current_game = jsonMsg.game;
                if (!jsonMsg.game) {
                    app.someUser.current_game = "Offline";
                }
                app.someUser.views = jsonMsg.views;
                app.someUser.channel_link = jsonMsg.url;
                app.someUser.last_updated = jsonMsg.updated_at;
            }
            console.log("displayName: " + app.displayName);
            console.log(msg);
            app.searchingBool = false;
        });
    };
    SingleTwitchComponent.prototype.searchUserChn = function () {
        var app = this;
        app.someUser = {
            display_name: "",
            broadcaster_language: "",
            channel_link: "",
            current_game: "",
            views: 0,
            followers: 0,
            logo: "",
            fresh_data: false,
            last_updated: "",
        };
        this.resetStatusValues();
        app.searchingBool = true;
        app.chnSearched = true;
        app.displayName = app.searchInput;
        app.statusMessage = "Searching...";
        var searchUrl = "/searchUserChn/" + app.searchInput;
        this.callAjax(searchUrl, function (msg) {
            var jsonMsg = JSON.parse(msg);
            if (jsonMsg.error) {
                // console.log("Error: " + jsonMsg.error);
                // console.log("No user by that name...");
                app.statusMessage = "No user by that name...";
            }
            else {
                app.displayName = jsonMsg.display_name;
                app.currentGame = "Offline";
                app.statusMessage = app.searchSuccess;
                app.someUser.display_name = jsonMsg.display_name;
                app.someUser.current_game = jsonMsg.game;
                if (jsonMsg.game == null) {
                    app.someUser.current_game = "Offline";
                }
                app.someUser.views = jsonMsg.views;
                app.someUser.channel_link = jsonMsg.url;
                app.someUser.last_updated = jsonMsg.updated_at;
                if (jsonMsg.status !== null) {
                    app.currentGame = "Streaming";
                }
            }
            // console.log("displayName: " + app.displayName);
            app.searchingBool = false;
            console.log(msg);
        });
    };
    SingleTwitchComponent.prototype.searchStreams = function () {
    };
    SingleTwitchComponent.prototype.callAjax = function (url, callback) {
        var xmlhttp;
        // compatible with IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                callback(xmlhttp.responseText);
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    };
    SingleTwitchComponent = __decorate([
        core_1.Component({
            selector: 'single-twitch',
            templateUrl: 'twitch/single-twitch.component.html',
            styleUrls: ['twitch/single-twitch.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], SingleTwitchComponent);
    return SingleTwitchComponent;
}());
exports.SingleTwitchComponent = SingleTwitchComponent;
//# sourceMappingURL=single-twitch.component.js.map