import { Component } from '@angular/core';
import { RouteParams } from '@angular/router-deprecated';

import { TwitchUser } from './twitch-user';
import { TwitchService } from './twitch.service';

@Component({
    selector: 'twitch-users',
    templateUrl: 'twitch/twitch-users.component.html',
    styleUrls: ['twitch/twitch-users.component.css']
})
export class TwitchUsersComponent {
    tUsers: TwitchUser[];
    searchTerm: string;
    constructor(
        private twitchService: TwitchService) { }
    searchUsers() {
        var app = this;
        var url =  '/search/' + this.searchTerm;
        app.tUsers = [];
//        console.log(url);
        this.twitchService.callAjax(url, function (msg) {
            var msgAsJSON = JSON.parse(msg);
            let arrayUsers = [];
            //console.log(msg);
            for (let i in msgAsJSON.channels){
                let user = new TwitchUser;
                user.display_name = msgAsJSON.channels[i].display_name;
                user.current_game = msgAsJSON.channels[i].game;
                user.followers = msgAsJSON.channels[i].followers;
                user.views = msgAsJSON.channels[i].views;
                user.logo = msgAsJSON.channels[i].logo;
                //arrayUsers.push(user);
                app.tUsers.push(user);
            }
            //app.tUsers = arrayUsers;
        })
    }
}