"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var TwitchSharedService = (function () {
    function TwitchSharedService() {
        // globalUsrList: TwitchUser[];
        this.globalUsrList = [{ display_name: "test1" }, { display_name: "test2" }, { display_name: "test3" }];
        this.globalNameList = [
            { display_name: "medrybw", status: "Unknown" },
            { display_name: "abbaba", status: "Unknown" },
            { display_name: "frankie", status: "Unknown" },
            { display_name: "eleaguetv", status: "Unknown" },
            { display_name: "eulcs2", status: "Unknown" },
            { display_name: "phantoml0rd", status: "Unknown" },
            { display_name: "starladder_hs_en", status: "Unknown" },
            { display_name: "summit1g", status: "Unknown" },
            { display_name: "reckful", status: "Unknown" },
            { display_name: "cartmanzbs", status: "Unknown" },
            { display_name: "elajjaz", status: "Unknown" },
            { display_name: "zeeoon", status: "Unknown" },
            { display_name: "cohhcarnage", status: "Unknown" },
            { display_name: "forsenlol", status: "Unknown" },
            { display_name: "rocketbeanstv", status: "Unknown" },
            { display_name: "summonersinnlive", status: "Unknown" },
            { display_name: "dansgaming", status: "Unknown" },
            { display_name: "savjz", status: "Unknown" },
            { display_name: "starladder5", status: "Unknown" },
            { display_name: "starladder_hs_ru", status: "Unknown" },
            { display_name: "flosd", status: "Unknown" },
            { display_name: "valkrin", status: "Unknown" },
            { display_name: "callofduty", status: "Unknown" },
            { display_name: "trumpsc", status: "Unknown" },
            { display_name: "troydangaming", status: "Unknown" },
            { display_name: "sardochelol", status: "Unknown" },
            { display_name: "esl_dota2", status: "Unknown" },
            { display_name: "imaqtpie", status: "Unknown" },
            { display_name: "garenatw", status: "Unknown" },
            { display_name: "esl_ruhub_dota2_ru", status: "Unknown" },
            { display_name: "thijshs", status: "Unknown" },
            { display_name: "cohhcarnage", status: "Unknown" },
            { display_name: "esl_ruhub_dota2b_ru", status: "Unknown" },
            { display_name: "phantoml0rd", status: "Unknown" },
            { display_name: "amazhs", status: "Unknown" },
            { display_name: "mobilmobil", status: "Unknown" },
            { display_name: "esl_joindotared", status: "Unknown" },
            { display_name: "timthetatman", status: "Unknown" },
            { display_name: "massansc", status: "Unknown" },
            { display_name: "nvidiatw", status: "Unknown" },
            { display_name: "underground_dv", status: "Unknown" },
            { display_name: "gaminglive_tv1", status: "Unknown" },
            { display_name: "eclypsiatvlol", status: "Unknown" },
            { display_name: "kinggothalion", status: "Unknown" },
            { display_name: "towelliee", status: "Unknown" },
            { display_name: "stray228", status: "Unknown" },
            { display_name: "miramisu", status: "Unknown" },
            { display_name: "tru3ta1ent", status: "Unknown" },
            { display_name: "iwilldominate", status: "Unknown" },
            { display_name: "rockraida", status: "Unknown" },
        ];
        this.testStr = "Test String";
    }
    TwitchSharedService.prototype.addToUsrList = function (usr) {
        this.globalUsrList.push(usr);
    };
    TwitchSharedService.prototype.getUserList = function () {
        return this.globalUsrList;
    };
    TwitchSharedService.prototype.addToUserList = function (user) {
        this.globalUsrList.push(user);
    };
    TwitchSharedService.prototype.getNameList = function () {
        return this.globalNameList;
    };
    TwitchSharedService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], TwitchSharedService);
    return TwitchSharedService;
}());
exports.TwitchSharedService = TwitchSharedService;
// export var globalUsrs=[{display_name:"test1"}, {display_name:"test2"}, {display_name:"test3"}];
// export var testStr = "Test String"; 
//# sourceMappingURL=twitch-shared.service.js.map