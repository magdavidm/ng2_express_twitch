import { Component } from '@angular/core';
import { RouteParams } from '@angular/router-deprecated';

import { TwitchUser } from './twitch-user';
import { TwitchService } from './twitch.service';

// import TwitchSharedService = require('./twitch-shared.service');
import { TwitchSharedService } from './twitch-shared.service';

@Component({
    selector: 'twitch-stream-single',
    templateUrl: 'twitch/twitch-stream-single.component.html',
    styleUrls: ['twitch/twitch-stream-single.component.css']
})

export class TwitchStreamSingleComponent {
    constructor(
        private twitchService: TwitchService,
        private twitchSharedService: TwitchSharedService) { }
    usersArr = this.twitchSharedService.getUserList();
    testStr = this.twitchSharedService.testStr;
    //sUser = TwitchUser;
    nameInput: string;
    singleUsr: TwitchUser;

    addUser(){
        // console.log(app.usersArr.length);
        var app = this;
        var user = new TwitchUser();
        // app.user.name = app.nameInput;
        var testUsr = {
            display_name: app.nameInput
        };
        user.display_name = app.nameInput;
        // app.usersArr.push(user);       
        // app.twitchSharedService.addToUserList(testUsr); 
        app.usersArr.push(testUsr);
        // app.singleUsr.display_name = app.nameInput;
        // app.usersArr.push(testUsr);
        // app.usersArr.push(app.singleUsr);  
        console.log(app.usersArr.length);
    }
}