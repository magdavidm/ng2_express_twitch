"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var twitch_user_1 = require('./twitch-user');
var twitch_service_1 = require('./twitch.service');
// import TwitchSharedService = require('./twitch-shared.service');
var twitch_shared_service_1 = require('./twitch-shared.service');
var TwitchStreamSingleComponent = (function () {
    function TwitchStreamSingleComponent(twitchService, twitchSharedService) {
        this.twitchService = twitchService;
        this.twitchSharedService = twitchSharedService;
        this.usersArr = this.twitchSharedService.getUserList();
        this.testStr = this.twitchSharedService.testStr;
    }
    TwitchStreamSingleComponent.prototype.addUser = function () {
        // console.log(app.usersArr.length);
        var app = this;
        var user = new twitch_user_1.TwitchUser();
        // app.user.name = app.nameInput;
        var testUsr = {
            display_name: app.nameInput
        };
        user.display_name = app.nameInput;
        // app.usersArr.push(user);       
        // app.twitchSharedService.addToUserList(testUsr); 
        app.usersArr.push(testUsr);
        // app.singleUsr.display_name = app.nameInput;
        // app.usersArr.push(testUsr);
        // app.usersArr.push(app.singleUsr);  
        console.log(app.usersArr.length);
    };
    TwitchStreamSingleComponent = __decorate([
        core_1.Component({
            selector: 'twitch-stream-single',
            templateUrl: 'twitch/twitch-stream-single.component.html',
            styleUrls: ['twitch/twitch-stream-single.component.css']
        }), 
        __metadata('design:paramtypes', [twitch_service_1.TwitchService, twitch_shared_service_1.TwitchSharedService])
    ], TwitchStreamSingleComponent);
    return TwitchStreamSingleComponent;
}());
exports.TwitchStreamSingleComponent = TwitchStreamSingleComponent;
//# sourceMappingURL=twitch-stream-single.component.js.map