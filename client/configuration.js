SystemJS.config({
            map: {
                "@angular": "node_modules/@angular",
                "rxjs": "node_modules/rxjs"
            },
            packages: {
                '/': {
                    //format: 'register',
                    defaultExtension: 'js'
                },
                'node_modules/@angular/http': {
                    //format: 'cjs',
                    defaultExtension: 'js',
                    main: 'http.js'
                },
                'node_modules/@angular/core': {
                    //format: 'cjs',
                    defaultExtension: 'js',
                    main: 'index.js'
                },
                'node_modules/@angular/router': {
                    //format: 'cjs',
                    defaultExtension: 'js',
                    main: 'index.js'
                },
                'node_modules/@angular/router-deprecated': {
                    //format: 'cjs',
                    defaultExtension: 'js',
                    main: 'index.js'
                },
                'node_modules/@angular/platform-browser-dynamic': {
                    //format: 'cjs',
                    defaultExtension: 'js',
                    main: 'index.js'
                },
                'node_modules/@angular/platform-browser': {
                    //format: 'cjs',
                    defaultExtension: 'js',
                    main: 'index.js'
                },
                'node_modules/@angular/compiler': {
                    //format: 'cjs',
                    defaultExtension: 'js',
                    main: 'compiler.js'
                },
                'node_modules/@angular/common': {
                    //format: 'cjs',
                    defaultExtension: 'js',
                    main: 'index.js'
                },
                'rxjs' : {
                    defaultExtension: 'js'
                }
            }
        });