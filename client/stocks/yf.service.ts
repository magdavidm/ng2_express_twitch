import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Stock } from './stock';
//import { YahooFinance } from 'yahoo-finance';
//var yahooFinance = require('yahoo-finance');

@Injectable()
export class YfService {
    private stocksUrl = 'app/stocks';  // URL to web api
    constructor(private http: Http) { }

    // getYfData(symbol: string): Promise<string> {
    //     //var yahooFinance = require('yahoo-finance');
    //     var data;
    //     yahooFinance.historical({
    //         symbol: 'AAPL',
    //         from: '2012-01-01',
    //         to: '2012-12-31',
    //         // period: 'd'  // 'd' (daily), 'w' (weekly), 'm' (monthly), 'v' (dividends only)
    //     })
    //     .toPromise()
    //     .then(response => data = JSON.stringify(response))
    //     .catch(this.handleError);
    //     ;
    //     return data;

    // }


    private handleError(error: any) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
