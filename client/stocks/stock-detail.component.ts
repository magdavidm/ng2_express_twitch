import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RouteParams } from '@angular/router-deprecated';
import { Stock }        from './stock';
import { StockService } from './stock.service';
@Component({
    selector: 'my-stock-detail',
    templateUrl: 'app/stocks/stock-detail.component.html',
    styleUrls: ['app/stocks/stock-detail.component.css']
})
export class StockDetailComponent implements OnInit {
    @Input() stock: Stock;
    @Output() close = new EventEmitter();
    error: any;
    navigated = false; // true if navigated here
    constructor(
        private stockService: StockService,
        private routeParams: RouteParams) {
    }
    ngOnInit() {
        if (this.routeParams.get('id') !== null) {
            let id = +this.routeParams.get('id');
            this.navigated = true;
            this.stockService.getStock(id)
                .then(stock => this.stock = stock);
        } else {
            this.navigated = false;
            this.stock = new Stock();
        }
    }
    save() {
        this.stockService
            .save(this.stock)
            .then(stock => {
                this.stock = stock;
                this.goBack(stock);
            })
            .catch(error => this.error = error); // TODO: Display error message
    }
    goBack(savedStock: Stock = null) {
        this.close.emit(savedStock);
        if (this.navigated) { window.history.back(); }
    }
}
