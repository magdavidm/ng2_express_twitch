"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var stock_service_1 = require('./stock.service');
var stock_detail_component_1 = require('./stock-detail.component');
var StockListComponent = (function () {
    function StockListComponent(router, stockService) {
        this.router = router;
        this.stockService = stockService;
        this.addingStock = false;
    }
    StockListComponent.prototype.getStocks = function () {
        var _this = this;
        this.stockService
            .getStocks()
            .then(function (stocks) { return _this.stocks = stocks; })
            .catch(function (error) { return _this.error = error; }); // TODO: Display error message
    };
    StockListComponent.prototype.ngOnInit = function () {
        this.getStocks();
    };
    StockListComponent.prototype.onSelect = function (stock) {
        this.selectedStock = stock;
        this.addingStock = false;
    };
    StockListComponent.prototype.gotoDetail = function () {
        this.router.navigate(['StockDetail', { id: this.selectedStock.id }]);
    };
    StockListComponent.prototype.addStock = function () {
        this.addingStock = true;
        this.selectedStock = null;
    };
    StockListComponent.prototype.close = function (savedStock) {
        this.addingStock = false;
        if (savedStock) {
            this.getStocks();
        }
    };
    StockListComponent.prototype.delete = function (stock, event) {
        var _this = this;
        event.stopPropagation();
        this.stockService
            .delete(stock)
            .then(function (res) {
            _this.stocks = _this.stocks.filter(function (s) { return s !== stock; });
            if (_this.selectedStock === stock) {
                _this.selectedStock = null;
            }
        })
            .catch(function (error) { return _this.error = error; }); // TODO: Display error message
    };
    StockListComponent = __decorate([
        core_1.Component({
            selector: 'my-stocks',
            templateUrl: 'app/stocks/stock-list.component.html',
            styleUrls: ['app/stocks/stock-list.component.css'],
            directives: [stock_detail_component_1.StockDetailComponent],
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, stock_service_1.StockService])
    ], StockListComponent);
    return StockListComponent;
}());
exports.StockListComponent = StockListComponent;
//# sourceMappingURL=stock-list.component.js.map