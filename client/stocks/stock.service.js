"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
var StockService = (function () {
    function StockService(http) {
        this.http = http;
        this.stocksUrl = 'app/stocks'; // URL to web api
    }
    StockService.prototype.getStocks = function () {
        return this.http.get(this.stocksUrl)
            .toPromise()
            .then(function (response) { return response.json().data; })
            .catch(this.handleError);
    };
    StockService.prototype.getStock = function (id) {
        return this.getStocks()
            .then(function (stocks) { return stocks.filter(function (stock) { return stock.id === id; })[0]; })
            .catch(this.handleError);
    };
    StockService.prototype.save = function (stock) {
        if (stock.id) {
            return this.put(stock);
        }
        return this.post(stock);
    };
    StockService.prototype.delete = function (stock) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var url = this.stocksUrl + "/" + stock.id;
        return this.http
            .delete(url, headers)
            .toPromise()
            .catch(this.handleError);
    };
    // Add new Stock
    StockService.prototype.post = function (stock) {
        var headers = new http_1.Headers({
            'Content-Type': 'application/json' });
        return this.http
            .post(this.stocksUrl, JSON.stringify(stock), { headers: headers })
            .toPromise()
            .then(function (res) { return res.json().data; })
            .catch(this.handleError);
    };
    // Update existing Stock
    StockService.prototype.put = function (stock) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var url = this.stocksUrl + "/" + stock.id;
        return this.http
            .put(url, JSON.stringify(stock), { headers: headers })
            .toPromise()
            .then(function () { return stock; })
            .catch(this.handleError);
    };
    StockService.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    StockService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], StockService);
    return StockService;
}());
exports.StockService = StockService;
//# sourceMappingURL=stock.service.js.map