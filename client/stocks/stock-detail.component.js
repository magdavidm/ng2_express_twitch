"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var stock_1 = require('./stock');
var stock_service_1 = require('./stock.service');
var StockDetailComponent = (function () {
    function StockDetailComponent(stockService, routeParams) {
        this.stockService = stockService;
        this.routeParams = routeParams;
        this.close = new core_1.EventEmitter();
        this.navigated = false; // true if navigated here
    }
    StockDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.routeParams.get('id') !== null) {
            var id = +this.routeParams.get('id');
            this.navigated = true;
            this.stockService.getStock(id)
                .then(function (stock) { return _this.stock = stock; });
        }
        else {
            this.navigated = false;
            this.stock = new stock_1.Stock();
        }
    };
    StockDetailComponent.prototype.save = function () {
        var _this = this;
        this.stockService
            .save(this.stock)
            .then(function (stock) {
            _this.stock = stock;
            _this.goBack(stock);
        })
            .catch(function (error) { return _this.error = error; }); // TODO: Display error message
    };
    StockDetailComponent.prototype.goBack = function (savedStock) {
        if (savedStock === void 0) { savedStock = null; }
        this.close.emit(savedStock);
        if (this.navigated) {
            window.history.back();
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', stock_1.Stock)
    ], StockDetailComponent.prototype, "stock", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], StockDetailComponent.prototype, "close", void 0);
    StockDetailComponent = __decorate([
        core_1.Component({
            selector: 'my-stock-detail',
            templateUrl: 'app/stocks/stock-detail.component.html',
            styleUrls: ['app/stocks/stock-detail.component.css']
        }), 
        __metadata('design:paramtypes', [stock_service_1.StockService, router_deprecated_1.RouteParams])
    ], StockDetailComponent);
    return StockDetailComponent;
}());
exports.StockDetailComponent = StockDetailComponent;
//# sourceMappingURL=stock-detail.component.js.map