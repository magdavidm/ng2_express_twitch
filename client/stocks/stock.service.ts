import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Stock } from './stock';
@Injectable()
export class StockService {
  private stocksUrl = 'app/stocks';  // URL to web api
  constructor(private http: Http) { }
  getStocks(): Promise<Stock[]> {
    return this.http.get(this.stocksUrl)
               .toPromise()
               .then(response => response.json().data)
               .catch(this.handleError);
  }
  getStock(id: number) {
    return this.getStocks()
               .then(stocks => stocks.filter(stock => stock.id === id)[0])
               .catch(this.handleError);
  }
  save(stock: Stock): Promise<Stock>  {
    if (stock.id) {
      return this.put(stock);
    }
    return this.post(stock);
  }
  delete(stock: Stock) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let url = `${this.stocksUrl}/${stock.id}`;
    return this.http
               .delete(url, headers)
               .toPromise()
               .catch(this.handleError);
  }
  // Add new Stock
  private post(stock: Stock): Promise<Stock> {
    let headers = new Headers({
      'Content-Type': 'application/json'});
    return this.http
               .post(this.stocksUrl, JSON.stringify(stock), {headers: headers})
               .toPromise()
               .then(res => res.json().data)
               .catch(this.handleError);
  }
  // Update existing Stock
  private put(stock: Stock) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let url = `${this.stocksUrl}/${stock.id}`;
    return this.http
               .put(url, JSON.stringify(stock), {headers: headers})
               .toPromise()
               .then(() => stock)
               .catch(this.handleError);
  }
  private handleError(error: any) {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
