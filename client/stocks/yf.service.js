"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
//import { YahooFinance } from 'yahoo-finance';
//var yahooFinance = require('yahoo-finance');
var YfService = (function () {
    function YfService(http) {
        this.http = http;
        this.stocksUrl = 'app/stocks'; // URL to web api
    }
    // getYfData(symbol: string): Promise<string> {
    //     //var yahooFinance = require('yahoo-finance');
    //     var data;
    //     yahooFinance.historical({
    //         symbol: 'AAPL',
    //         from: '2012-01-01',
    //         to: '2012-12-31',
    //         // period: 'd'  // 'd' (daily), 'w' (weekly), 'm' (monthly), 'v' (dividends only)
    //     })
    //     .toPromise()
    //     .then(response => data = JSON.stringify(response))
    //     .catch(this.handleError);
    //     ;
    //     return data;
    // }
    YfService.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    YfService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], YfService);
    return YfService;
}());
exports.YfService = YfService;
//# sourceMappingURL=yf.service.js.map