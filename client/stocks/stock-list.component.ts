import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router-deprecated';
import { Stock }                from './stock';
import { StockService }         from './stock.service';
import { StockDetailComponent } from './stock-detail.component';
@Component({
  selector: 'my-stocks',
  templateUrl: 'app/stocks/stock-list.component.html',
  styleUrls:  ['app/stocks/stock-list.component.css'],
  directives: [StockDetailComponent],
})
export class StockListComponent implements OnInit {
  stocks: Stock[];
  selectedStock: Stock;
  addingStock = false;
  error: any;
  constructor(
    private router: Router,
    private stockService: StockService) { }
  getStocks() {
    this.stockService
        .getStocks()
        .then(stocks => this.stocks = stocks)
        .catch(error => this.error = error); // TODO: Display error message
  }

  ngOnInit() {
    this.getStocks();
  }
  onSelect(stock: Stock) {
    this.selectedStock = stock;
    this.addingStock = false;
  }
  gotoDetail() {
    this.router.navigate(['StockDetail', { id: this.selectedStock.id }]);
  }
  addStock() {
    this.addingStock = true;
    this.selectedStock = null;
  }
  close(savedStock: Stock) {
    this.addingStock = false;
    if (savedStock) { this.getStocks(); }
  }
  delete(stock: Stock, event: any) {
    event.stopPropagation();
    this.stockService
        .delete(stock)
        .then(res => {
          this.stocks = this.stocks.filter(s => s !== stock);
          if (this.selectedStock === stock) { this.selectedStock = null; }
        })
        .catch(error => this.error = error); // TODO: Display error message
  }
}
