export class Stock {
    id: number;
    name: string;
    value: number;
    ticker: string;
}