"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var dashboard_component_1 = require('./dashboard/dashboard.component');
var single_twitch_component_1 = require('./twitch/single-twitch.component');
var twitch_users_component_1 = require('./twitch/twitch-users.component');
var twitch_stream_single_component_1 = require('./twitch/twitch-stream-single.component');
var twitch_stream_multi_component_1 = require('./twitch/twitch-stream-multi.component');
var twitch_service_1 = require('./twitch/twitch.service');
var router_deprecated_1 = require('@angular/router-deprecated');
var AppComponent = (function () {
    function AppComponent() {
        this.title = "NG2 TwitchTV User App";
    }
    AppComponent = __decorate([
        router_deprecated_1.RouteConfig([
            {
                path: '/dashboard',
                name: 'Dashboard',
                component: dashboard_component_1.DashboardComponent,
            },
            {
                path: '/singleTwitch',
                name: 'SingleTwitch',
                component: single_twitch_component_1.SingleTwitchComponent,
            },
            {
                path: '/twitchUsers',
                name: 'TwitchUsers',
                component: twitch_users_component_1.TwitchUsersComponent,
            },
            {
                path: '/twitchStreamSingle',
                name: 'TwitchStreamSingle',
                component: twitch_stream_single_component_1.TwitchStreamSingleComponent,
            },
            {
                path: '/twitchStreamMulti',
                name: 'TwitchStreamMulti',
                component: twitch_stream_multi_component_1.TwitchStreamMultiComponent,
            },
        ]),
        core_1.Component({
            selector: 'main-app',
            template: "\n    <h1>{{title}}</h1>\n    <nav>\n    <a [routerLink]=\"['Dashboard']\">Dashboard</a>\n    <a [routerLink]=\"['SingleTwitch']\">Single User</a>\n    <a [routerLink]=\"['TwitchUsers']\">Twitch Users</a>\n    <a [routerLink]=\"['TwitchStreamSingle']\">SingleStream</a>\n    <a [routerLink]=\"['TwitchStreamMulti']\">MultiStream</a>\n    </nav>\n    <router-outlet></router-outlet>\n    ",
            styleUrls: ['app.component.css'],
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [
                router_deprecated_1.ROUTER_PROVIDERS,
                twitch_service_1.TwitchService
            ],
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map