import {Component} from '@angular/core';

import { DashboardComponent } from './dashboard/dashboard.component';
import { SingleTwitchComponent } from './twitch/single-twitch.component';
import { TwitchUsersComponent } from './twitch/twitch-users.component';
import { TwitchStreamSingleComponent } from './twitch/twitch-stream-single.component';
import { TwitchStreamMultiComponent } from './twitch/twitch-stream-multi.component';

import { TwitchService } from './twitch/twitch.service';

import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated';


@RouteConfig([
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: DashboardComponent,
    },
    {
        path: '/singleTwitch',
        name: 'SingleTwitch',
        component: SingleTwitchComponent,
    },
    {
        path: '/twitchUsers',
        name: 'TwitchUsers',
        component: TwitchUsersComponent,
    },
    {
        path: '/twitchStreamSingle',
        name: 'TwitchStreamSingle',
        component: TwitchStreamSingleComponent,
    },
    {
        path: '/twitchStreamMulti',
        name: 'TwitchStreamMulti',
        component: TwitchStreamMultiComponent,
    },
])

@Component({
    selector: 'main-app',
    template: `
    <h1>{{title}}</h1>
    <nav>
    <a [routerLink]="['Dashboard']">Dashboard</a>
    <a [routerLink]="['SingleTwitch']">Single User</a>
    <a [routerLink]="['TwitchUsers']">Twitch Users</a>
    <a [routerLink]="['TwitchStreamSingle']">SingleStream</a>
    <a [routerLink]="['TwitchStreamMulti']">MultiStream</a>
    </nav>
    <router-outlet></router-outlet>
    `,
    styleUrls: ['app.component.css'],
    directives: [ROUTER_DIRECTIVES],
    providers: [
        ROUTER_PROVIDERS,
        TwitchService
    ],
    
})
export class AppComponent {
    title = "NG2 TwitchTV User App"
}