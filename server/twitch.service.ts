const $https = require('https');

export class TwitchService {
    //twitchChn = 'https://api.twitch.tv/kraken/search/channels?query=medrybw?';
    twitchChn = 'https://api.twitch.tv/kraken/search/channels?query=';
    twitchUsrChn = 'https://api.twitch.tv/kraken/channels/';
    twitchUsr = 'https://api.twitch.tv/kraken/users/';
    twitchStream = 'https://api.twitch.tv/kraken/streams/';

    retrieveChannels(user: string, callback) {
        var twitchQuery = this.twitchChn + user;
        $https.get(twitchQuery, (res) => {
            let body = '';
            res.on('data', (d) => {
                body += d;
            });
            res.on('end', function () {
                //console.log(typeof(body));
                callback(body);
                // var jsonBody = JSON.parse(body);
                // console.log(typeof(jsonBody));
                // callback(jsonBody);

            });
        }).on('error', (e) => {
            console.log('An error occured: ' + e.message);
        });
    }
    retrieveUser(user: string, callback) {
        var twitchQuery = this.twitchUsr + user;
        $https.get(twitchQuery, (res) => {
            let body = '';
            res.on('data', (d) => {
                body += d;
            });
            res.on('end', function () {
                callback(body);
            });
        }).on('error', (e) => {
            console.log('An error occured: ' + e.message);
        });
    }
    retrieveUserChannel(user: string, callback) {
        var twitchQuery = this.twitchUsrChn + user;
        $https.get(twitchQuery, (res) => {
            let body = '';
            res.on('data', (d) => {
                body += d;
            });
            res.on('end', function () {
                callback(body);
            });
        }).on('error', (e) => {
            console.log('An error occured: ' + e.message);
        });
    }
    retrieveStreamData(user: string, callback) {
        var twitchQuery = this.twitchStream + user;
        $https.get(twitchQuery, (res) => {
            let body = '';
            res.on('data', (d) => {
                body += d;
            });
            res.on('end', function () {
                callback(body);
            });
        }).on('error', (e) => {
            console.log('An error occured: ' + e.message);
        });
    }
}