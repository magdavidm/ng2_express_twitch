"use strict";
var $https = require('https');
var TwitchService = (function () {
    function TwitchService() {
        //twitchChn = 'https://api.twitch.tv/kraken/search/channels?query=medrybw?';
        this.twitchChn = 'https://api.twitch.tv/kraken/search/channels?query=';
        this.twitchUsrChn = 'https://api.twitch.tv/kraken/channels/';
        this.twitchUsr = 'https://api.twitch.tv/kraken/users/';
        this.twitchStream = 'https://api.twitch.tv/kraken/streams/';
    }
    TwitchService.prototype.retrieveChannels = function (user, callback) {
        var twitchQuery = this.twitchChn + user;
        $https.get(twitchQuery, function (res) {
            var body = '';
            res.on('data', function (d) {
                body += d;
            });
            res.on('end', function () {
                //console.log(typeof(body));
                callback(body);
                // var jsonBody = JSON.parse(body);
                // console.log(typeof(jsonBody));
                // callback(jsonBody);
            });
        }).on('error', function (e) {
            console.log('An error occured: ' + e.message);
        });
    };
    TwitchService.prototype.retrieveUser = function (user, callback) {
        var twitchQuery = this.twitchUsr + user;
        $https.get(twitchQuery, function (res) {
            var body = '';
            res.on('data', function (d) {
                body += d;
            });
            res.on('end', function () {
                callback(body);
            });
        }).on('error', function (e) {
            console.log('An error occured: ' + e.message);
        });
    };
    TwitchService.prototype.retrieveUserChannel = function (user, callback) {
        var twitchQuery = this.twitchUsrChn + user;
        $https.get(twitchQuery, function (res) {
            var body = '';
            res.on('data', function (d) {
                body += d;
            });
            res.on('end', function () {
                callback(body);
            });
        }).on('error', function (e) {
            console.log('An error occured: ' + e.message);
        });
    };
    TwitchService.prototype.retrieveStreamData = function (user, callback) {
        var twitchQuery = this.twitchStream + user;
        $https.get(twitchQuery, function (res) {
            var body = '';
            res.on('data', function (d) {
                body += d;
            });
            res.on('end', function () {
                callback(body);
            });
        }).on('error', function (e) {
            console.log('An error occured: ' + e.message);
        });
    };
    return TwitchService;
}());
exports.TwitchService = TwitchService;
//# sourceMappingURL=twitch.service.js.map